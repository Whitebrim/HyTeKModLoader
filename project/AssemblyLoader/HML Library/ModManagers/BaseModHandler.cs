﻿using AssemblyLoader;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace HMLLibrary
{
    public enum ExitCode : int
    {
        None = -1,
        Success = 0,
        CompileFailed = 1,
        ParseError = 2,
        ReferenceError = 4
    }

    public class BaseModHandler : MonoBehaviour
    {
        public static Dictionary<string, ModData> dataCache = new Dictionary<string, ModData>();

        public virtual async Task<ModData> GetModData(FileInfo file) { throw new NotImplementedException(); }

        public virtual async Task<Assembly> GetModAssembly(ModData moddata)
        {
            ExitCode progress = ExitCode.None;
            Assembly assembly = null;

            if (!moddata.modinfo.isShortcut)
            {
                string potentialCachedVersion = Path.Combine(HLib.path_cacheFolder_mods, moddata.modinfo.fileHash + "_f.dll");
                if (File.Exists(potentialCachedVersion))
                {
                    try
                    {
                        assembly = Assembly.Load(File.ReadAllBytes(potentialCachedVersion));
                        return assembly;
                    }
                    catch { }
                }
            }

            if (ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name))
            {
                ModManagerPage.loadedAssemblies.Remove(moddata.modinfo.modFile.Name);
            }
            moddata.modinfo.modState = ModInfo.ModStateEnum.compiling;
            ModManagerPage.RefreshModState(moddata);

#if GAME_IS_GREENHELL
            string TempDirectory = Path.Combine(HLib.path_cacheFolder_temp, "tempfix_mod_"+moddata.modinfo.modFile.Name);
            Directory.CreateDirectory(TempDirectory);

            moddata.modinfo.modFiles.ToList().Where(t => t.Key.ToLower().EndsWith(".cs")).ToList().ForEach((modfile) =>
            {
                File.WriteAllBytes(Path.Combine(TempDirectory, Path.GetFileName(modfile.Key)), modfile.Value);
            });

            if (moddata.modinfo.modFiles.ToList().Where(t => t.Key.ToLower().EndsWith(".cs")).ToList().Count == 1)
            {
                var filename = moddata.modinfo.modFiles.ToList().Where(t => t.Key.ToLower().EndsWith(".cs")).First();
                var file = Path.Combine(TempDirectory, Path.GetFileName(filename.Key));
                if (File.Exists(file))
                {
                    string content = File.ReadAllText(file);
                    if (content.Contains("using Harmony;"))
                    {
                        Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod is using a deprecated version of <b>Harmony</b> (Harmony Namespace)! Trying to automatically repair it...");
                        content = content.Replace("using Harmony;", "using HarmonyLib;");
                        File.WriteAllText(file, content);
                        if (HUtils.AddFileToMod(moddata, file, filename.Key))
                        {
                            Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > Replaced <b>using Harmony;</b> with <b>using HarmonyLib;</b> to hopefully repair the mod! Trying to compile...");
                            string potentialCachedVersion = Path.Combine(HLib.path_cacheFolder_mods, moddata.modinfo.fileHash + ".dll");
                            if (File.Exists(potentialCachedVersion))
                            {
                                File.Delete(potentialCachedVersion);
                            }
                            moddata.modinfo.modFiles[filename.Key] = File.ReadAllBytes(file);
                        }
                    }

                    if (content.Contains("HarmonyInstance"))
                    {
                        Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod is using a deprecated version of <b>Harmony</b> (HarmonyInstance Type)! Trying to automatically repair it...");

                        content = content.Replace("HarmonyInstance.Create", "new Harmony");
                        content = content.Replace("HarmonyInstance", "Harmony");
                        File.WriteAllText(file, content);
                        if (HUtils.AddFileToMod(moddata, file, filename.Key))
                        {
                            Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > Replaced <b>HarmonyInstance.Create</b> with <b>new Harmony</b> and <b>HarmonyInstance</b> with <b>Harmony</b> to hopefully repair the mod! Trying to compile...");
                            string potentialCachedVersion = Path.Combine(HLib.path_cacheFolder_mods, moddata.modinfo.fileHash + ".dll");
                            if (File.Exists(potentialCachedVersion))
                            {
                                File.Delete(potentialCachedVersion);
                            }
                            moddata.modinfo.modFiles[filename.Key] = File.ReadAllBytes(file);
                        }
                    }
                }
            }
#endif

#if GAME_IS_RAFT
            string TempDirectory = Path.Combine(HLib.path_cacheFolder_temp, "tempfix_mod_" + moddata.modinfo.modFile.Name);
            Directory.CreateDirectory(TempDirectory);
            moddata.modinfo.modFiles.ToList().Where(t => t.Key.ToLower().EndsWith(".cs")).ToList().ForEach((modfile) =>
            {
                File.WriteAllBytes(Path.Combine(TempDirectory, Path.GetFileName(modfile.Key)), modfile.Value);
            });
            if (moddata.modinfo.modFiles.ToList().Where(t => t.Key.ToLower().EndsWith(".cs")).ToList().Count == 1)
            {
                var filename = moddata.modinfo.modFiles.ToList().Where(t => t.Key.ToLower().EndsWith(".cs")).First();
                var file = Path.Combine(TempDirectory, Path.GetFileName(filename.Key));
                if (File.Exists(file))
                {
                    string content = File.ReadAllText(file);
                    if (content.Contains("Semih_Network.InLobbyScene"))
                    {
                        Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod is using an old raft field (Semih_Network.InLobbyScene)! Trying to automatically repair it...");
                        content = content.Replace("Semih_Network.InLobbyScene", "RAPI.IsCurrentSceneMainMenu()");
                        File.WriteAllText(file, content);
                        if (HUtils.AddFileToMod(moddata, file, filename.Key))
                        {
                            Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > Replaced <b>Semih_Network.InLobbyScene</b> with <b>RAPI.IsCurrentSceneMainMenu()</b> to hopefully repair the mod! Trying to compile...");
                            string potentialCachedVersion = Path.Combine(HLib.path_cacheFolder_mods, moddata.modinfo.fileHash + ".dll");
                            if (File.Exists(potentialCachedVersion))
                            {
                                File.Delete(potentialCachedVersion);
                            }
                            moddata.modinfo.modFiles[filename.Key] = File.ReadAllBytes(file);
                        }
                    }

                    /*if (content.Contains("HarmonyInstance"))
                    {
                        Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod is using a deprecated version of <b>Harmony</b> (HarmonyInstance Type)! Trying to automatically repair it...");

                        content = content.Replace("HarmonyInstance.Create", "new Harmony");
                        content = content.Replace("HarmonyInstance", "Harmony");
                        File.WriteAllText(file, content);
                        if (HUtils.AddFileToMod(moddata, file, filename.Key))
                        {
                            Debug.LogWarning("[ModCompiler] " + moddata.modinfo.modFile.Name + " > Replaced <b>HarmonyInstance.Create</b> with <b>new Harmony</b> and <b>HarmonyInstance</b> with <b>Harmony</b> to hopefully repair the mod! Trying to compile...");
                            string potentialCachedVersion = Path.Combine(HLib.path_cacheFolder_mods, moddata.modinfo.fileHash + ".dll");
                            if (File.Exists(potentialCachedVersion))
                            {
                                File.Delete(potentialCachedVersion);
                            }
                            moddata.modinfo.modFiles[filename.Key] = File.ReadAllBytes(file);
                        }
                    }*/
                }
            }
#endif

            if (HyTeKInjector.csc == null || HyTeKInjector.csc.HasExited)
            {
                Debug.LogError("[ModCompiler] The mod compiler process is not running! Trying to start csc.exe...");
                HyTeKInjector.StartCSC();
            }

            DataReceivedEventHandler eventHandler = null;
            string log = "";
            ExitCode exitCode = ExitCode.None;
            eventHandler = (object sender, DataReceivedEventArgs e) =>
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() =>
                {
                    if (e.Data == null)
                    {
                        Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to compile!\nReceived empty event data from the compiler!");
                    }
                    else
                    {
                        if (e.Data.StartsWith(moddata.modinfo.fileHash))
                        {
                            log = Encoding.UTF8.GetString(Convert.FromBase64String(e.Data.Remove(0, moddata.modinfo.fileHash.Length + 1))); // Removes check code and whitespace or \n next to it
                            if (log.Length == 1)
                            {
                                exitCode = (ExitCode)int.Parse(log);
                                if (exitCode == ExitCode.Success)
                                {
                                    try
                                    {
                                        assembly = Assembly.Load(File.ReadAllBytes(Path.Combine(HLib.path_cacheFolder_mods, moddata.modinfo.fileHash + "_f.dll")));
                                    }
                                    catch (Exception ex)
                                    {
                                        Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to load!\n" + ex);
                                        moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                                        ModManagerPage.RefreshModState(moddata);
                                    }
                                }
                                AssemblyLoader.HyTeKInjector.csc.OutputDataReceived -= eventHandler;
                                progress = exitCode;
                            }
                            else
                            {
                                Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to compile!\n" + log);
                                moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                                ModManagerPage.RefreshModState(moddata);
                            }
                        }
                    }
                });
            };

            AssemblyLoader.HyTeKInjector.csc.OutputDataReceived += eventHandler;
            AssemblyLoader.HyTeKInjector.csc.StandardInput.WriteLine(string.Format("-s \"{0}\" -n \"{1}\" -o \"{2}\" -r \"{3}\" \"{4}\" -c \"{5}\"",
                moddata.modinfo.modFile, // Input
                moddata.modinfo.fileHash, // Assembly name
                Path.Combine(HLib.path_cacheFolder_mods, moddata.modinfo.fileHash + "_f.dll"), // Output
                Application.dataPath + "/Managed", // Referenced assemblies
                HLib.path_binariesFolder, // Referenced assemblies
                moddata.modinfo.fileHash)); // Async check string

            int waitingTime = 0;

            while (progress == ExitCode.None && waitingTime < 15000)
            {
                waitingTime += 5;
                await Task.Delay(5);
            }

            AssemblyLoader.HyTeKInjector.csc.OutputDataReceived -= eventHandler;
            return assembly;
        }

        public virtual async void LoadMod(ModData moddata)
        {
            try
            {
                if (moddata == null || moddata.modinfo.modState == ModInfo.ModStateEnum.running || moddata.modinfo.modState == ModInfo.ModStateEnum.compiling) { return; }
                if (!HLib.CanLoadMod(moddata))
                {
                    HNotify.Get().AddNotification(HNotify.NotificationType.normal, moddata.jsonmodinfo.name + " can't be loaded while players are online!", 5, await HLib.bundle.TaskLoadAssetAsync<Sprite>("IconError"));
                    return;
                }
                bool modHasUpdated = moddata.modinfo.isShortcut ? moddata.modinfo.fileHash != HUtils.CreateSHA512ForFolder(moddata.modinfo.shortcutFolder) : moddata.modinfo.fileHash != HUtils.GetFileSHA512Hash(moddata.modinfo.modFile.FullName);
                if (modHasUpdated)
                {
                    await ModManagerPage.RefreshMod(moddata.modinfo.modFile);
                    moddata = ModManagerPage.modList.Find((m) => m.modinfo.modFile.Name == moddata.modinfo.modFile.Name);
                }
                if (!ModManagerPage.ModsGameObjectParent.transform.Find(moddata.modinfo.modFile.Name.ToLower()))
                {
                    if (modHasUpdated || moddata.modinfo.assembly == null) { moddata.modinfo.assembly = await GetModAssembly(moddata); }
                    if (!ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name))
                    {
                        ModManagerPage.loadedAssemblies.Add(moddata.modinfo.modFile.Name, moddata.modinfo.assembly);
                    }
                    else
                    {
                        ModManagerPage.loadedAssemblies[moddata.modinfo.modFile.Name] = moddata.modinfo.assembly;
                    }
                    if (moddata.modinfo.assembly == null) { moddata.modinfo.modState = ModInfo.ModStateEnum.errored; }
                    else
                    {
                        IEnumerable<Type> types = moddata.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                        if (types.Count() != 1)
                        {
                            Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + "> The mod codebase doesn't specify a mod class or specifies more than one!");
                            moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                        }
                        else
                        {
                            GameObject ModObj = new GameObject();
                            ModObj.SetActive(false);
                            ModObj.name = moddata.modinfo.modFile.Name.ToLower();
                            ModObj.transform.parent = ModManagerPage.ModsGameObjectParent.transform;
                            moddata.modinfo.mainClass = ModObj.AddComponent(types.FirstOrDefault()) as Mod;
                            moddata.modinfo.mainClass.modlistEntry = moddata;
                            ModObj.SetActive(true);
                            moddata.modinfo.goInstance = ModObj;
                            moddata.modinfo.modState = ModInfo.ModStateEnum.running;
                        }
                    }
                }
                if (modHasUpdated)
                    ModManagerPage.RefreshMod(moddata.modinfo.modFile);
                else
                    ModManagerPage.RefreshModState(moddata);
            }
            catch (Exception e)
            {
                moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
            }
            HConsole.instance.RefreshCommands();
        }

        public virtual async void UnloadMod(ModData moddata)
        {
            try
            {
                if(!HLib.CanUnloadMod(moddata.jsonmodinfo.name, moddata.jsonmodinfo.version))
                {
                    HNotify.Get().AddNotification(HNotify.NotificationType.normal, moddata.jsonmodinfo.name + " is required by the server and can't be unloaded!", 5, await HLib.bundle.TaskLoadAssetAsync<Sprite>("IconError"));
                    return;
                }

                if (moddata.modinfo.modState == ModInfo.ModStateEnum.running)
                {
                    Transform mod = ModManagerPage.ModsGameObjectParent.transform.Find(moddata.modinfo.modFile.Name.ToLower());
                    try
                    {
                        if (mod != null && moddata.modinfo.assembly != null)
                        {
                            IEnumerable<Type> types = moddata.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                            if (mod.GetComponent(types.First()))
                            {
                                MethodInfo methodInfo = AccessTools.Method(types.First(), "OnModUnload");
                                if (methodInfo != null)
                                    if (methodInfo.IsStatic) { methodInfo.Invoke(null, null); } else { methodInfo.Invoke(mod.GetComponent(types.First()), null); }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > An error occured while running the mod OnModUnload() method!\nStacktrace : " + e.ToString());
                    }
                    if (mod != null)
                    {
                        Destroy(mod.gameObject);
                    }
                    if (ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name)) { ModManagerPage.loadedAssemblies.Remove(moddata.modinfo.modFile.Name); }
                    moddata.modinfo.modState = ModInfo.ModStateEnum.idle;
                    ModManagerPage.RefreshModState(moddata);
                    HConsole.instance.RefreshCommands();
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > An error occured while unloading the mod!\nStacktrace : " + e.ToString());
            }
        }
    }
}