﻿using HMLLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using Debug = UnityEngine.Debug;
namespace GHML
{
    public class RawSharp : MonoBehaviour
    {
        public static string evalPrefix = "<color=#2b67cf>[Evaluation]</color> ";

        [ConsoleCommand("csrun", "Syntax : 'csrun <code...>' Allows you to run CSharp code at runtime for testing and debugging.")]
        public static void CSRUN(string[] args)
        {
            if (args.Length < 1)
            {
                Debug.LogWarning("You must specify code to evaluate with 'csrun'.");
                return;
            }
            string evalCode = string.Join(" ", args);
            if (evalCode.Length < 2)
            {
                Debug.LogWarning("You must specify code to evaluate with 'csrun'.");
                return;
            }
            Debug.Log(evalPrefix + "Evaluation is in progress...");
            EvaluateCode(evalCode);
        }

        public static async void EvaluateCode(string evalCode)
        {
            DateTime start = DateTime.Now;
            Assembly assembly = null;
            string evalFilePath = Path.Combine(HLib.path_cacheFolder_temp, "eval.cs");
            File.WriteAllText(evalFilePath, evalFileContent.Replace("EVALCODE_STRING", evalCode));

            System.Diagnostics.DataReceivedEventHandler eventHandler = null;
            string log = "";
            ExitCode progress = ExitCode.None;
            ExitCode exitCode = ExitCode.None;
            string hash = HUtils.GetFileSHA512Hash(evalFilePath);
            string outPath = string.Format("{0}\\evalassembly_{1}.dll", HLib.path_cacheFolder_temp, DateTime.Now.Ticks);
            eventHandler = (object sender, System.Diagnostics.DataReceivedEventArgs e) =>
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() =>
                {
                    if (e.Data.StartsWith(hash))
                    {
                        log = Encoding.UTF8.GetString(Convert.FromBase64String(e.Data.Remove(0, hash.Length + 1))); // Removes check code and whitespace or \n next to it
                        if (log.Length == 1)
                        {
                            exitCode = (ExitCode)int.Parse(log);
                            if (exitCode == ExitCode.Success)
                            {
                                try
                                {
                                    assembly = Assembly.Load(File.ReadAllBytes(outPath));
                                    var methodInfo = assembly.GetTypes().First().GetMethod("EvalMethod");
                                    methodInfo.Invoke(null, null);
                                    Debug.Log(evalPrefix + "Evaluation succeeded in " + (DateTime.Now - start).Milliseconds + "ms!");
                                }
                                catch (Exception ex)
                                {
                                    Debug.LogError(evalPrefix + " The evaluation failed!\n" + ex);
                                }
                            }
                            AssemblyLoader.HyTeKInjector.csc.OutputDataReceived -= eventHandler;
                            progress = exitCode;
                        }
                        else
                        {
                            Debug.LogError(evalPrefix + " The evaluation failed!\n" + log);
                        }
                    }
                });
            };
            AssemblyLoader.HyTeKInjector.csc.OutputDataReceived += eventHandler;
            AssemblyLoader.HyTeKInjector.csc.StandardInput.WriteLine(string.Format("-s \"{0}\" -n \"{1}\" -o \"{2}\" -r \"{3}\" \"{4}\" -c \"{5}\"",
                new FileInfo(evalFilePath), // Input
                hash, // Assembly name
                outPath, // Output
                Application.dataPath + "/Managed", // Referenced assemblies
                HLib.path_binariesFolder, // Referenced assemblies
                hash)); // Async check string

            int waitingTime = 0;

            while (progress == ExitCode.None && waitingTime < 5000)
            {
                waitingTime += 5;
                await Task.Delay(5);
            }

            AssemblyLoader.HyTeKInjector.csc.OutputDataReceived -= eventHandler;
            if (File.Exists(evalFilePath)) { File.Delete(evalFilePath); }
            if (File.Exists(outPath)) { File.Delete(outPath); }
        }

        public static string evalFileContent = @"using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using HarmonyLib;
using Steamworks;
using UnityEngine.AI;
using System.Net.Sockets;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine.UI;
using System.Collections;
using GHML;
using Enums;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
using Object = UnityEngine.Object;

public class Class1 : MonoBehaviour
{
    public static void EvalMethod()
    {
        EVALCODE_STRING
    }
}";
    }
}